/* 
 * tstPWM1 - dsPIC33FJ256GP710
 *
 * This is a walk and chew gum sort of an exercise.  LED's
 * D4-D9 get toggled frequently based on the loop counter.
 * D3 gets toggled based on Timer 1.
 *
 * Timer 3 is used to control the PWM on OC3.  The duty cycle is
 * slowly increased, causing a LED on an external board to slowly
 * become dimmer.  When the duty cycle gets to 100%, LED D10 is
 * toggled, and the duty cycle gets reset to 0.
 *
 * Meanwhile, OC7 is being used to control another external
 * LED.  This LED's duty cycle is based on the position of the
 * onboard potentiometer.
 *
 * The A/D converter is set to read the potentiometer continuously 
 * and interrupt whenever the conversion is complete.  The A/D
 * ISR simply grabs the value and stuffs it in a global to be used
 * later by OC3.
 *
 * jjmcd 2011-06-16
 */

#include <stdio.h>
#include <stdlib.h>
#include <p33Fxxxx.h>

_FOSCSEL(FNOSC_PRIPLL);   // Primary oscillator PLL
_FOSC(FCKSM_CSDCMD & OSCIOFNC_OFF & POSCMD_XT);
_FWDT(FWDTEN_OFF);

#define PR1MAX 20000      // Initial value for Timer 1
volatile unsigned int potValue; // Position of pot

/*
 * Initilization
 */
void Initialize( void )
{
    /* set LEDs (D3-D10/RA0-RA7) alternate LEDs on */
    LATA = 0xFF55;
    /* set LED pins (D3-D10/RA0-RA7) as outputs */
    TRISA = 0xFF00;
    /* Set Proto board LEDS all off */
    LATD = 0xFFFF;
    /* set proto board LEDS RD2 & RD6 as outputs */
    TRISD = 0xFFBB;

    // Timer used to flash LED in interrupt context
    TMR1 = 0;             // clear timer 1
    PR1 = PR1MAX;         // interrupt every PR1MAX ticks
    IFS0bits.T1IF = 0;    // clr interrupt flag
    IEC0bits.T1IE = 1;    // set interrupt enable bit
    T1CON = 0x8030;       // Fosc/4, 1:256 prescale, start TMR1

    // Set up PWM on OC7 (RD6)
    TMR2 = 0;             // Clear timer 2
    PR2 = 1000;           // Timer 2 counter to 1000
    OC7RS = 999;          // PWM 7 almost to PR2
    OC7R = 999;           //   sets duty cycle almost zero
    OC7CON = 0x6;         // Set OC 7 to PWM mode
    T2CON = 0x8010;       // Fosc/4, 1:4 prescale, start TMR2

    // Set up PWM on OC3 (RD2)
    TMR3 = 0;             // Clear timer 2
    PR3 = 1000;           // Timer 2 counter to 1000
    OC3RS = 0;            // PWM 3 duty cycle 0
    OC3R = 0;             //
    OC3CON = 0x6;         // Set OC 7 to PWM mode
    T3CON = 0x8010;       // Fosc/4, 1:4 prescale, start TMR2

    // Initialize A/D converter
    /* set port configuration here */
    AD1PCFGLbits.PCFG4 = 0;         // ensure AN4/RB4 is analog (Temp Sensor)
    AD1PCFGLbits.PCFG5 = 0;         // ensure AN5/RB5 is analog (Analog Pot)
    /* set channel scanning here, auto sampling and convert,
       with default read-format mode */
    AD1CON1 = 0x00E4;
    /* select 12-bit, 1 channel ADC operation */
    AD1CON1bits.AD12B = 1;
    /* No channel scan for CH0+, Use MUX A,
       SMPI = 1 per interrupt, Vref = AVdd/AVss */
    AD1CON2 = 0x0000;
    /* Set Samples and bit conversion time */
    AD1CON3 = 0x032F;
    /* set channel scanning here for AN4 and AN5 */
    AD1CSSL = 0x0000;
    /* channel select AN5 */
    AD1CHS0 = 0x0005;
    /* reset ADC interrupt flag */
    IFS0bits.AD1IF = 0;
    /* enable ADC interrupts */
    IEC0bits.AD1IE = 1;
    /* turn on ADC module */
    AD1CON1bits.ADON = 1;
}

/*
 * Waste time
 */
void delay(void) {
    long i = 64000;
    while(i--)
        ;
}

/*
 * Mainline
 */
int main(int argc, char** argv) {

    Initialize();

    while(1) {
        LATA ^= 0x7e;     // Toggle LEDs D4-D9
        delay();          // wait

        OC3RS += 10;
        OC7RS = potValue/4;      // And increment OC3
        if ( OC3RS > 990 ) // If almost always on
        {
            LATA ^= 0x80; //Toggle LED D10
            OC3RS = 0;
        }
    }
    return (EXIT_SUCCESS);
}

void __attribute__((interrupt, no_auto_psv)) _T1Interrupt(void)
{
    IFS0bits.T1IF = 0;    // clear timer interrupt flag
    LATA ^= 0x01;         //Toggle LED D3 (RA0)
}

void __attribute__((__interrupt__, auto_psv)) _ADC1Interrupt( void )
{
  IFS0bits.AD1IF = 0;    // Clear A/D interrupt flag
  potValue = ADC1BUF0;   // Save the potentiometer value
}
